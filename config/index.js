// index.js - figure out what set of configs to return
if (process.env.NODE_ENV === 'production') {
  // production mode - return test configs
  module.exports = require('./prod');
} else if (process.env.NODE_ENV === 'test') {
  // test mode - return test configs
  module.exports = require('./test');
} else {
  // dev mode - return the dev configs!
  module.exports = require('./dev');
}
