const mongoose = require('mongoose');
const { Schema } = mongoose; // Imported this way to avoid problems with unit tests
const config = require('../config');

const calcSchema = new Schema(
  {
    ptOption: { type: String },
    bigData: { type: String },
    mvpRange: Schema.Types.Mixed,
    prjRange: Schema.Types.Mixed,
    prjFunc: Schema.Types.Mixed,
    prjHost: Schema.Types.Mixed,
    prjData: Schema.Types.Mixed,
    prefLang: Schema.Types.Mixed,
    mvpComplexity: { type: Number },
    customLevel: { type: Number },
    users: { type: Number },
    deadlines: Schema.Types.Mixed,
    year1MonthExp: Schema.Types.Mixed,
    year2MonthExp: Schema.Types.Mixed,
  },
  {
    timestamps: true,
    usePushEach: true
  }
);

// methods on userSchema

// calculate total year 1 expenses based on monthly expenses
calcSchema.virtual('year1TotExp').get(function () {
  const expObj = this.year1MonthExp;
  return Object.keys(expObj).reduce(
    (total, key) => total += expObj[key],
    0
  )
});

// calculate total year 1 expenses based on monthly expenses
calcSchema.virtual('year1TotExp').get(function () {
  const expObj = this.year1MonthExp;
  return Object.keys(expObj).reduce(
    (total, key) => total += expObj[key],
    0
  )
});

mongoose.model('calcInput', calcSchema);
