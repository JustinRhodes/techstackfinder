const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const validator = require('express-validator');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const config = require('./config');
require('./models/Calculator');

// set up mongoDB connection
mongoose.Promise = global.Promise;
mongoose.connect(config.mongoURL);
mongoose.connection.on('error', () => {
  console.log('MongoDB connection error. Is MongoDB up & running?');
  process.exit(1);
});

// set up express application
const app = express();

const http = require('http').Server(app);

// adding security/protection to server
app.use(helmet({ frameguard: { action: 'sameorigin' } }));
app.disable('x-powered-by'); //obscuring we are using express

// dev mode: log every request & serve openAPI docs
if (process.env.NODE_ENV !== 'production' || process.env.NODE_ENV !== 'test') {
  app.use(morgan('dev'));
  app.get('/v1/redoc', (req, res) => {
    res.sendFile(__dirname + '/redoc.html');
  });
  app.get('/v1/redoc/swagger', (req, res) => {
    res.sendFile(__dirname + '/swagger.yaml');
  });
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(validator());

// plugging in routes from routes folder
app.use('/v1/calculator', require('./routes/calculator'));

if (process.env.NODE_ENV === 'production') {
  // serve build assets in production mode
  app.use(express.static('client/build'));
  // serve index.html if route not recognized
  const path = require('path');
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

module.exports = { http, app };
