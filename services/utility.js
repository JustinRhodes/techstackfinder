const config = require('../config');

exports.msg = {
    invalidInput: 'Invalid input',
    success: 'operation successful',
    internalServerError: 'Internal server error',
};
