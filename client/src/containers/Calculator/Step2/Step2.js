import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ExpansionPanel, { ExpansionPanelDetails, ExpansionPanelSummary } from 'material-ui/ExpansionPanel';
import Typography from 'material-ui/Typography';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import { withStyles } from 'material-ui/styles';

import Button from '../../../components/UI/Button/Default/Default';
import Slider from '../../../components/UI/Slider/Slider';
import Select from '../../../components/UI/Select/Single/Single';
import Container from '../../../components/UI/Container/Container';
import StreamGraph from '../../../components/UI/Stream/Stream';

import { yearOptions, monthOptions } from '../../../shared/options';
import { commaFormatted } from '../../../shared/utility';

import css from './Step2.module.css';

/*This component (& the step 1 component) could also be turned 
purely presentational, thus removing local state completely. I 
have left the local state here as it's not relevant to other parts 
of the app*/

const styles = theme => ({
    heading: {
        fontSize: theme.typography.pxToRem(13),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(13),
        color: theme.palette.text.secondary,
    },
});

class Step2 extends Component {
    state = {
        expanded: 'panel1'
    }

    handlePanelExpand = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };

    renderTotalBudget(budgetObj) {
        return Object.keys(budgetObj).reduce(
            (total, key) => total += budgetObj[key],
            0
        )
    }

    renderDeadlines(yPrefix, mPrefix, tSuffix, array) {
        let { input, onChangeData } = this.props;
        const timeSelectBlock = array.map(option => {
            if (option === 'Nothing to Select' || typeof (option) === 'undefined') return null;
            return (
                <div key={option} className={css.DeadlineWrapper}>
                    <span className={css.DeadlineInfo}>
                        <strong>{option}</strong>{tSuffix}
                    </span>
                    <div className={css.InputOuterWrapper}>
                        <div className={css.InputInnerWrapper}>
                            <Select
                                ariaLabel={option}
                                name={option}
                                options={yearOptions}
                                value={input.deadlines[`${yPrefix}_${option}`] || ''}
                                handleChange={onChangeData}
                                stateProperty={`${yPrefix}_${option}`}
                                nestedObject={'deadlines'}
                                placeholder={'Please select year'}
                                label={'Year of implementation'}
                                helperText={'In which year should the goal be achieved?'}
                                helpLink={'/'}
                                tipTrigger={false}
                                tipText={``}
                            />
                        </div>
                        <div className={css.InputInnerWrapper}>
                            <Select
                                ariaLabel={option}
                                name={option}
                                options={monthOptions}
                                value={input.deadlines[`${mPrefix}_${option}`] || ''}
                                handleChange={onChangeData}
                                stateProperty={`${mPrefix}_${option}`}
                                nestedObject={'deadlines'}
                                placeholder={'Please select month'}
                                label={'Month of implementation'}
                                helperText={'In which month should the goal be achieved?'}
                                helpLink={'/'}
                                tipTrigger={false}
                                tipText={``}
                            />
                        </div>
                    </div>
                </div>
            )
        })
        return timeSelectBlock;
    }

    renderGraphSliders(array, data) {
        let { input, onChangeData } = this.props;
        const sliders = array.map(month => {
            const defaultValue =
                input[data][month.value] ? input[data][month.value] / 1000 : null
            return (
                <div key={month.value} className={css.SliderWrapper}>
                    <p className={css.SubTitle}>
                        {month.label}
                    </p>
                    <Slider
                        tipSign={'k'}
                        defaultValue={defaultValue}
                        max={100}
                        onAfterChange={(value) => onChangeData(month.value, value * 1000, data)}
                    />
                </div>
            )
        })
        return sliders;
    }

    render() {
        const { expanded } = this.state;

        const {
            classes,
            history,
            onSendData,
            input
        } = this.props;

        const {
            prjRange,
            mvpRange,
            year1MonthExp,
            year2MonthExp
        } = this.props.input;

        const timeSelectTip = mvpRange.length === 0 && prjRange.length === 0 ?
            'Select a product range from step 1 to set deadlines' : null;
        const deadlinesMvp = this.renderDeadlines('ymvp', 'mmvp', ' (MVP)', mvpRange);
        const deadlinesPrj = this.renderDeadlines('y', 'm', ' (Full Product)', prjRange);
        const slidersYear1 = this.renderGraphSliders(monthOptions, 'year1MonthExp');
        const slidersYear2 = this.renderGraphSliders(monthOptions, 'year2MonthExp');
        const totalBudgetYear1 = this.renderTotalBudget(year1MonthExp);
        const totalBudgetYear2 = this.renderTotalBudget(year2MonthExp);

        return (
            < Container
                background={'#fafafa'}
            >
                <div className={css.Title}>
                    <h1><strong>Step2: </strong>Defining your Project Structure</h1>
                </div>
                <ExpansionPanel
                    expanded={expanded === 'panel1'}
                    onChange={this.handlePanelExpand('panel1')}
                >
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>
                            <strong>1st Year: </strong>Technical Project Budget (monthly)
                        </Typography>
                        <Typography className={classes.secondaryHeading}>
                            Budget for technical implementations
                        </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={css.BudgetDetails}>
                        <div className={css.BudgetWrapper}>
                            <p>
                                <strong>Total spending (year 1): ${commaFormatted(totalBudgetYear1)}</strong>
                            </p>
                            <div className={css.StreamWrapper}>
                                <StreamGraph
                                    data={year1MonthExp}
                                    array={monthOptions}
                                    xKey={'month'}
                                    yKey={'budget'}
                                />
                            </div>
                        </div>
                        <div className={css.SliderGroup}>
                            {slidersYear1}
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel
                    expanded={expanded === 'panel2'}
                    onChange={this.handlePanelExpand('panel2')}
                >
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>
                            <strong>2nd Year: </strong>Technical Project Budget (monthly)
                        </Typography>
                        <Typography className={classes.secondaryHeading}>
                            Budget for technical implementations
                        </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={css.BudgetDetails}>
                        <div className={css.BudgetWrapper}>
                            <p>
                                <strong>Total spending (year 2): ${commaFormatted(totalBudgetYear2)}</strong>
                            </p>
                            <div className={css.StreamWrapper}>
                                <StreamGraph
                                    data={year2MonthExp}
                                    array={monthOptions}
                                    xKey={'month'}
                                    yKey={'budget'} />
                            </div>
                        </div>
                        <div className={css.SliderGroup}>
                            {slidersYear2}
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel
                    expanded={expanded === 'panel3'}
                    onChange={this.handlePanelExpand('panel3')}
                >
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>
                            Project Deadlines
                        </Typography>
                        <Typography className={classes.secondaryHeading}>
                            Deadlines by which you want your product(s)/MVP implemented
                        </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <div className={css.Deadlines}>
                            {deadlinesMvp}
                            {deadlinesPrj}
                            {timeSelectTip}
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <div className={css.ButtonDiv}>
                    <Button onClick={() => history.push('/calculator/step1')}>
                        Back
                    </Button>
                    <Button
                        onClick={() => onSendData(input, 'step2', 'step3', history)}
                    >
                        Continue
                    </Button>
                </div>
            </ Container >
        );
    }
}

export default withRouter(withStyles(styles)(Step2));