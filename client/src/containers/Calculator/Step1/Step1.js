import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ExpansionPanel, { ExpansionPanelDetails, ExpansionPanelSummary } from 'material-ui/ExpansionPanel';
import Typography from 'material-ui/Typography';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import { withStyles } from 'material-ui/styles';

import Select from '../../../components/UI/Select/Single/Single';
import MultiSelect from '../../../components/UI/Select/Multi/Multi';
import Button from '../../../components/UI/Button/Default/Default';
import Slider from '../../../components/UI/Slider/Slider';
import Container from '../../../components/UI/Container/Container';
import Transition from '../../../components/Transition/Transition';

import * as _ from '../../../shared/options';

import css from './Step1.module.css';

/*This component (& the step 2 component) could also be turned 
purely presentational, thus removing local state completely. I 
have left the local state here as it's not relevant to other parts 
of the app*/

const styles = theme => ({
    heading: {
        fontSize: theme.typography.pxToRem(13),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(13),
        color: theme.palette.text.secondary,
    },
});

class Step1 extends Component {
    state = {
        expanded: 'panel1'
    };

    handlePanelExpand = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };

    render() {
        const { expanded } = this.state;
        const {
            classes,
            onChangeData,
            onDeleteData,
            onSendData,
            history,
            input
        } = this.props;

        const {
            prjRange,
            mvpRange,
            ptOption,
            bigData,
            prjFunc,
            prjHost,
            prjData,
            prefLang,
            mvpComplexity,
            users
        } = this.props.input;

        let mvpRangeTip = null;
        if (prjRange.length === 0) {
            mvpRangeTip =
                `Please add products to your product range to use this function`;
        }
        if (mvpRange.length > 1) {
            mvpRangeTip =
                `Tip: Focus on one (the simplest) product for your MVP`;
        }

        return (
            < Container
                background={'#fafafa'}
            >
                <div className={css.Title}>
                    <h1><strong>Step1: </strong>Defining your idea's technical requirements</h1>
                </div>

                <ExpansionPanel expanded={expanded === 'panel1'} onChange={this.handlePanelExpand('panel1')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Final Product Overview (Part 1)</Typography>
                        <Typography className={classes.secondaryHeading}>Technical project range</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={css.Details}>
                        <div className={css.DetailsContainer}>
                            <div className={css.Wrapper}>
                                <MultiSelect
                                    options={_.longTermOptions}
                                    selValues={prjRange}
                                    handleChange={event => onChangeData('prjRange', event.target.value)}
                                    handleDelete={onDeleteData}
                                    stateProperty={'prjRange'}
                                    label={'Product Range'}
                                    helperText={'What does your final product range include?'}
                                    helpLink={'/'}
                                    tipTrigger={false}
                                    tipText={``}
                                />
                            </div>
                            <div className={css.Wrapper}>
                                <MultiSelect
                                    options={_.prjFuncOptions}
                                    selValues={prjFunc}
                                    handleChange={event => onChangeData('prjFunc', event.target.value)}
                                    handleDelete={onDeleteData}
                                    stateProperty={'prjFunc'}
                                    label={'Product Functionalities'}
                                    helperText={'Which functions should your product include?'}
                                    helpLink={'/'}
                                    tipTrigger={false}
                                    tipText={``}
                                />
                            </div>
                            <div className={css.Wrapper}>
                                <MultiSelect
                                    options={_.prjHostingOptions}
                                    selValues={prjHost}
                                    handleChange={event => onChangeData('prjHost', event.target.value)}
                                    handleDelete={onDeleteData}
                                    stateProperty={'prjHost'}
                                    label={'Hosting Provider'}
                                    helperText={'Where do you want to host your product(s)?'}
                                    helpLink={'/'}
                                    tipTrigger={false}
                                    tipText={``}
                                />
                            </div>
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel2'} onChange={this.handlePanelExpand('panel2')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Final Product Overview (Part 2)</Typography>
                        <Typography className={classes.secondaryHeading}>Technical preferences/customization</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={css.Details}>
                        <div className={css.DetailsContainer}>
                            <div className={css.Wrapper}>
                                <MultiSelect
                                    options={_.prefLangOptions}
                                    selValues={prefLang}
                                    handleChange={event => onChangeData('prefLang', event.target.value)}
                                    handleDelete={onDeleteData}
                                    stateProperty={'prefLang'}
                                    label={'Preferred Language(s)'}
                                    helperText={'Do you have any preferred language you would like to use?'}
                                    helpLink={'/'}
                                    tipTrigger={prefLang.length > 2}
                                    tipText={`Tip: Try to stick to a minimum of languages.`}
                                />
                            </div>
                            <div className={css.Wrapper}>
                                <Slider
                                    sliderHeader={'Product uniqueness/customization'}
                                    tipSign={'%'}
                                    max={100}
                                    onAfterChange={value => onChangeData('customLevel', value)}
                                    helperText={`Level of customization: 0% = standard functionalities; 
                                            100% = absolutely unique.`}
                                    helpLink={'/'}
                                    tipTrigger={false}
                                    tipText={``}
                                />
                            </div>
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel3'} onChange={this.handlePanelExpand('panel3')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Final Product Overview (Part 3)</Typography>
                        <Typography className={classes.secondaryHeading}>
                            Product data type(s) &amp; usage
                            </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={css.Details}>
                        <div className={css.DetailsContainer}>
                            <div className={css.Wrapper}>
                                <MultiSelect
                                    options={_.prjDataOptions}
                                    selValues={prjData}
                                    handleChange={event => onChangeData('prjData', event.target.value)}
                                    handleDelete={onDeleteData}
                                    stateProperty={'prjData'}
                                    label={'Do you mainly...'}
                                    helperText={'What type of data do(es) your product(s) use?'}
                                    helpLink={'/'}
                                    tipTrigger={false}
                                    tipText={``}
                                />
                            </div>
                            <div className={css.Wrapper}>
                                <Select
                                    ariaLabel="bigData"
                                    name="bigData"
                                    options={_.bigDataOptions}
                                    value={bigData}
                                    handleChange={onChangeData}
                                    stateProperty={'bigData'}
                                    placeholder={'Please select option'}
                                    label={'Are you using big data?'}
                                    helperText={'Big data is very large e.g. millions of data entries.'}
                                    helpLink={'/'}
                                    tipTrigger={false}
                                    tipText={``}
                                />
                            </div>
                            <div className={css.Wrapper}>
                                <Slider
                                    sliderHeader={' How many monthly users do you expect?'}
                                    tipSign={'k'}
                                    max={300}
                                    onAfterChange={value => onChangeData('users', value)}
                                    helperText={`Expected users per month in thousand.`}
                                    helpLink={'/'}
                                    tipTrigger={users === 0}
                                    tipText={`Tip: Expect between 20k & 100k users/month`}
                                />
                            </div>
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel4'} onChange={this.handlePanelExpand('panel4')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Minimum Variable Product (MVP)</Typography>
                        <Typography className={classes.secondaryHeading}>
                            What should it include? By when do you need it?
                            </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={css.Details}>
                        <div className={css.DetailsContainer}>
                            <div className={css.Wrapper}>
                                <MultiSelect
                                    options={prjRange}
                                    selValues={mvpRange}
                                    handleChange={event => onChangeData('mvpRange', event.target.value)}
                                    handleDelete={onDeleteData}
                                    stateProperty={'mvpRange'}
                                    label={'MVP Range'}
                                    helperText={'What should your MVP(s) include?'}
                                    helpLink={'/'}
                                    tipTrigger={
                                        mvpRange.length > 1 ||
                                        prjRange.length === 0
                                    }
                                    tipText={mvpRangeTip}
                                />
                            </div>
                            <div className={css.Wrapper}>
                                <Select
                                    ariaLabel="prototype"
                                    name="prototype"
                                    options={_.prototypeOptions}
                                    value={ptOption}
                                    handleChange={onChangeData}
                                    stateProperty={'ptOption'}
                                    placeholder={'Please select option'}
                                    label={'MVP interactivity'}
                                    helperText={'How interactive should your MVP(s) be?'}
                                    helpLink={'/'}
                                    tipTrigger={ptOption === 'intWCode'}
                                    tipText={`Tip: You can crate an interactive prototype without 
                                coding to show to investors`}
                                />
                            </div>
                            < Transition show={ptOption === 'intWCode'}>
                                <div className={css.Wrapper}>
                                    <Slider
                                        sliderHeader={'MVP sophistication (coded MVPs)'}
                                        tipSign={'%'}
                                        max={100}
                                        defaultValue={mvpComplexity}
                                        onAfterChange={value => onChangeData('mvpComplexity', value)}
                                        helperText={`Level of sophistication: 0% = no functionality; 
                                            100% = market-ready product.`}
                                        helpLink={'/'}
                                        tipTrigger={mvpComplexity > 30}
                                        tipText={`Tip: Try to keep the MVP as simple 
                                            as possible (below 30%)`}
                                    />
                                </div>
                            </Transition >
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <div className={css.ButtonDiv}>
                    <Button
                        onClick={() => onSendData(input, 'step1', 'step2', history)}
                    >
                        Continue
                    </Button>
                </div>
            </ Container >
        );
    }
}

export default withRouter(withStyles(styles)(Step1));