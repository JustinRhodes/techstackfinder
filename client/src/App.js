import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import Layout from './components/Layout/Layout';
import Hero from './components/Hero/Hero';
import Step1 from './containers/Calculator/Step1/Step1';
import Step2 from './containers/Calculator/Step2/Step2';
import Page404 from './components/Page404/Page404';

import * as actions from './store/actions';

/* This component acts as the app's main container, linking
state & dispatch events to props which are then passed down to the 
sub-components. Alternatively, these props could be directly 
accessed in the step1 & step2 sub-components, thus turning them
into true containers (beyond local state containers)*/

class App extends Component {
  render() {

    const {
      prjRange,
      mvpRange
    } = this.props.data;

    const {
      deadlines,
      year1MonthExp,
      year2MonthExp,
      calcId,
      ...step1Data
    } = this.props.data;

    const step2Data = {
      prjRange,
      mvpRange,
      deadlines,
      year1MonthExp,
      year2MonthExp,
      calcId
    }

    let routes = (
      <Switch>
        <Route path="/" exact component={Hero} />
        <Route
          path="/calculator/step1"
          exact
          render={() =>
            <Step1
              input={step1Data}
              onChangeData={this.props.onChangeData}
              onDeleteData={this.props.onDeleteData}
              onSendData={this.props.onSendData}
            />
          }
        />
        <Route
          path="/calculator/step2"
          exact
          render={() =>
            <Step2
              input={step2Data}
              onChangeData={this.props.onChangeData}
              onSendData={this.props.onSendData}
            />
          }
        />
        <Route component={Page404} />
      </Switch>
    );
    return <Layout>{routes}</Layout>;
  }
}

const mapStateToProps = state => {
  return {
    data: state.calc
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onChangeData: (property, data, nestedObj) => dispatch(actions.changeData(property, data, nestedObj)),
    onDeleteData: (property, data) => dispatch(actions.deleteData(property, data)),
    onSendData: (data, step, nextStep, history) => dispatch(actions.sendData(data, step, nextStep, history))
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));