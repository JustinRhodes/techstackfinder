// options relating to Step 1

export const longTermOptions = [
    { value: 'ws', label: 'Website' },
    { value: 'wa', label: 'Web App' },
    { value: 'pwa', label: 'Progressive Web App' },
    { value: 'm_app', label: 'Mobile App' },
    { value: 'd_app', label: 'Desktop App' }
]

export const prototypeOptions = [
    { value: 'intNoCode', label: 'Interactive, but not coded/functional' },
    { value: 'intWCode', label: 'Interactive, with functional code' },
    { value: 'wf', label: 'Non-interactive wireframes' },
]

export const prjFuncOptions = [
    { value: 'imgUpl', label: 'Image Upload' },
    { value: 'emails', label: 'Email System' },
    { value: 'r_upds', label: 'Instant, Realtime Updates' },
    { value: 'chat', label: 'Chat System' },
    { value: 'pdf', label: 'PDF System' },
    { value: 'd_calc', label: 'Data Analysis/Calculations' },
    { value: 'ops', label: 'Online Payment System' },
    { value: 'mp', label: 'Market Place' },
    { value: 'blog', label: 'Blog System' },
    { value: 'bs', label: 'Booking System' },
    { value: 'sms', label: 'SMS System' },
    { value: 'gps', label: 'GPS System' },
    { value: 'file', label: 'File System' },
    { value: 'u_dash', label: 'User Dashboard' },
    { value: 'graphs', label: 'Data Graphs' },
    { value: 'anm', label: 'Animations' },
    { value: 'socials', label: 'Social Share System' },
    { value: 'p_not', label: 'Push Notifications' },
    { value: 'login', label: 'Login System' },
    { value: 'admin', label: 'Admin System & Dashboard' },
    { value: 'o/i', label: 'Store, Edit & Retrieve User Data' },
    { value: 'search', label: 'Search Data' },
    { value: 'u_acc', label: 'User Account System' }
]

export const prjHostingOptions = [
    { value: 'aws', label: 'Amazon Web Services (AWS)' },
    { value: 'her', label: 'Heroku' },
    { value: 'm_az', label: 'Microsoft Azure' },
    { value: 'm_app_e', label: 'Microsoft App Engine' },
    { value: 'other', label: 'Other' }
]

export const prjDataOptions = [
    { value: 'noRel', label: 'Store & Retrieve data' },
    { value: 'sql', label: 'Analyze & Calculate data' },
    { value: 'graph', label: 'Use data networks' },
]

export const bigDataOptions = [
    { value: 'y', label: 'Yes' },
    { value: 'n', label: 'No' },
    { value: 'p', label: 'Partially' },
]

export const prefLangOptions = [
    { value: 'py', label: 'Python' },
    { value: 'js', label: 'JavaScript' },
    { value: 'php', label: 'PHP' },
    { value: 'jav', label: 'Java' },
    { value: 'swf', label: 'Swift' },
    { value: 'obc', label: 'Objective-C' },
    { value: 'go', label: 'Golang' }
]

// options relating to Step 2

export const yearOptions = [
    { value: 'y1', label: 'Year 1' },
    { value: 'y2', label: 'Year 2' }
]

export const monthOptions = [
    { value: 'Jan', label: 'January' },
    { value: 'Feb', label: 'February' },
    { value: 'Mar', label: 'March' },
    { value: 'Apr', label: 'April' },
    { value: 'May', label: 'May' },
    { value: 'Jun', label: 'June' },
    { value: 'Jul', label: 'July' },
    { value: 'Aug', label: 'August' },
    { value: 'Sep', label: 'September' },
    { value: 'Oct', label: 'October' },
    { value: 'Nov', label: 'November' },
    { value: 'Dec', label: 'December' }
]