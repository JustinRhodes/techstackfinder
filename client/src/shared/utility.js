/* Helper functions */

/**
 * Copies objects immutably with new values
 * @param {object} oldObject - Object to copy.
 * @param {mixed} updatedValues - Values to update object with
 * @returns {object} - New copy of object with values
 */
export const updateObject = (oldObject, updatedValues) => {
  return {
    ...oldObject,
    ...updatedValues
  };
};

/**
* Removes keys immutably from object by creating copy
* @param {object} object - Object to copy.
* @param {string} key - Object key to remove
* @returns {object} - New copy of object without key
*/
export const copyWithoutKey = (object, key) => {
  const { [key]: deletedKey, ...otherKeys } = object;
  return otherKeys;
};

/**
* Reformats numbers by adding comma delineation to numbers >1000
* @param {number} number - Number to reformat
* @returns {string} - String converted from number with comma delineation
*/
export const commaFormatted = number => {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/**
* Checks if email format is valid
* @param {string} email - Email to check
* @returns {boolean} - True or false depending on email
*/
export const invalidEmail = email => {
  const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  return !pattern.test(email);
};

/**
* Checks if password is valid (>8 char, 1 upperCase)
* @param {string} password - Password to check
* @returns {boolean} - True or false depending on password
*/
export const invalidPassword = password => {
  const pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
  return !pattern.test(password);
};