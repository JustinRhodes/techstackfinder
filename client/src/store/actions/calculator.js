import axios from 'axios';
import * as actionTypes from './actionTypes';

export const changeData = (property, value, nestedObject) => {
  return {
    type: actionTypes.CHANGE_DATA,
    property,
    value,
    nestedObject
  };
};

export const deleteData = (property, value) => {
  return {
    type: actionTypes.DELETE_DATA,
    property,
    value
  };
};

export const sendData = (data, step, nextStep, history) => {
  return dispatch => {
    axios
      .post('/v1/calculator/' + step, data)
      .then(response => {
        dispatch(changeData('calcId', response.data.id));
        history.push('/calculator/' + nextStep);
      })
      .catch(err => {
        console.log(err);
      });
  };
};