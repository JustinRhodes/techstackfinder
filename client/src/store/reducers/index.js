import { combineReducers } from 'redux';
// import exampleReducer from './example';
import calcReducer from './calculator';

export default combineReducers({
  calc: calcReducer,
  // exmpl: exampleReducer // JR: this is how I would add more reducers
});
