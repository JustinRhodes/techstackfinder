import * as actionTypes from '../actions/actionTypes';
// import { updateObject } from '../../shared/utility';

const initialState = {
  ptOption: '',
  bigData: '',
  mvpRange: [],
  prjRange: [],
  prjFunc: [],
  prjHost: [],
  prjData: [],
  prefLang: [],
  mvpComplexity: 0,
  customLevel: 0,
  users: 0,
  deadlines: {},
  year1MonthExp: {},
  year2MonthExp: {},
  calcId: '',
};

const changeData = (state, action) => {
  if (action.nestedObject) {
    return {
      ...state,
      [action.nestedObject]: {
        ...state[action.nestedObject],
        [action.property]: action.value
      }
    }
  }
  return {
    ...state,
    [action.property]: action.value
  }
}

const deleteData = (state, action) => {
  const valueArray = [...state[action.property]];
  const chipToDelete = valueArray.indexOf(action.value);
  valueArray.splice(chipToDelete, 1);
  return {
    ...state,
    [action.property]: valueArray
  }
}


const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_DATA:
      return changeData(state, action);
    case actionTypes.DELETE_DATA:
      return deleteData(state, action);
    default:
      return state;
  }
};

export default reducer;
