import React from 'react';

import Button from '../UI/Button/Default/Default';
import * as css from './Hero.module.css';

const Hero = props => {
    return (
        <section
            className={css.Hero}
        >
            <div className={css.HeroContainer}>
                <p> Give your startup a chance, it's FREE!</p>
                <p className={css.HeroTitle}>
                    Find Web Development technologies <br />
                    to fit your ideas
                </p>
                <p> "XXX" calculates the perfect TechStack for your startup<br />
                    entirely customized and made to order
                </p>
                <Button onClick={() => props.history.push('/calculator/step1')}>
                    Try it out (FREE)
                </Button>
            </div>
        </section>
    );
};

export default Hero;