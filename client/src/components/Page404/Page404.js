import React from 'react';
import { withRouter } from 'react-router-dom';
import * as css from './Page404.module.css'

const Comp404 = props => {
    return (
        <div className={css.OuterWrapper}>
            <div className={css.InnerWrapper}>
                <h1 className={css.Title}>
                    <strong>404</strong><br />
                    Page not found.
                </h1>
                <p className={css.Text}>
                    Click
                    <span
                        className={css.Link}
                        onClick={() => props.history.goBack()}
                    >
                        {' here '}
                    </span>
                    to get back
                </p>

            </div>
        </div>
    );
};

export default withRouter(Comp404);