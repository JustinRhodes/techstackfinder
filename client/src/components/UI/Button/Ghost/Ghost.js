import React from 'react';
import Button from 'material-ui/Button';
import PropTypes from 'prop-types';

import css from './Ghost.module.css';

const GhostBtn = props => {
  return (
    <Button
      className={css.Button}
      {...props}
    >
      {props.children}
    </Button >
  );
};

GhostBtn.propTypes = {
  onClick: PropTypes.func.isRequired
};

export default GhostBtn;