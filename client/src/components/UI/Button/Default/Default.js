import React from 'react';
import Button from 'material-ui/Button';
import PropTypes from 'prop-types';

import css from './Default.module.css';

const DefaultBtn = (props) => {
    return (
        <Button
            className={css.Button}
            variant='raised'
            {...props}
        >
            {props.children}
        </Button >
    );
};

DefaultBtn.propTypes = {
    onClick: PropTypes.func.isRequired
};

export default DefaultBtn;