import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Select from 'material-ui/Select';

import Transition from '../../../../components/Transition/Transition';
import HelpLink from '../../../HelpLink/HelpLink';
import css from './Single.module.css';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        width: '100%',
    }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        }
    }
};

const SimpleSelect = props => {
    const { classes } = props;
    const options = props.options.map(option => {
        return (
            <MenuItem key={option.value} value={option.value}>
                {option.label}
            </MenuItem>
        )
    });
    const helpLink = props.helpLink ?
        <HelpLink helpLink={props.helpLink} /> : null;

    return (
        <form className={classes.root} autoComplete="off">
            <FormControl className={classes.formControl}>
                <InputLabel htmlFor="select-helper">{props.label}</InputLabel>
                <Select
                    value={props.value}
                    onChange={event =>
                        props.handleChange(props.stateProperty, event.target.value, props.nestedObject)}
                    input={<Input name="select" id="select-helper" />}
                    MenuProps={MenuProps}
                >
                    <MenuItem value=''>
                        <em>{props.placeholder}</em>
                    </MenuItem>
                    {options}
                </Select>
                <FormHelperText>
                    {props.helperText}
                    {helpLink}
                </FormHelperText>
                < Transition
                    show={props.tipTrigger}
                >
                    <FormHelperText>
                        <span className={css.Tip}>
                            <strong>{props.tipText}</strong>
                        </span>
                    </FormHelperText>
                </ Transition>
            </FormControl>
        </form>
    );
}

SimpleSelect.propTypes = {
    classes: PropTypes.object.isRequired,
    options: PropTypes.array.isRequired,
    value: PropTypes.string.isRequired,
    stateProperty: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    helpLink: PropTypes.string,
    helperText: PropTypes.string,
    tipTrigger: PropTypes.bool,
    tipText: PropTypes.string
};

export default withStyles(styles)(SimpleSelect);