import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Input, { InputLabel } from 'material-ui/Input';
import blue from 'material-ui/colors/blue';
import { MenuItem } from 'material-ui/Menu';
import { FormControl, FormHelperText } from 'material-ui/Form';
import { ListItemText } from 'material-ui/List';
import Select from 'material-ui/Select';
import Checkbox from 'material-ui/Checkbox';

import Transition from '../../../../components/Transition/Transition';
import Chip from '../../Chip/Chip';
import HelpLink from '../../../HelpLink/HelpLink';
import css from './Multi.module.css';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        width: '100%',
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit / 4,
    },
    checkbox: {
        color: blue[700],
    }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        }
    }
};

export const MultipleSelect = props => {
    const { classes } = props;
    const helpLink = props.helpLink ?
        <HelpLink helpLink={props.helpLink} /> : null;

    let MenuOptions = (
        <MenuItem>
            <ListItemText primary={'Nothing to Select'} />
        </MenuItem>
    )

    if (props.options.length) {
        MenuOptions = props.options.map(option => (
            <MenuItem key={option.value || option} value={option.label || option}>
                <Checkbox
                    checked={props.selValues.indexOf(option.label || option) > -1}
                    className={classes.checkbox}
                />
                <ListItemText primary={option.label || option} />
            </MenuItem>
        ))
    }

    return (
        <div className={classes.root}>
            <FormControl className={classes.formControl}>
                <InputLabel htmlFor="select-multiple-chip">{props.label}</InputLabel>
                <Select
                    multiple
                    value={props.selValues}
                    onChange={props.handleChange}
                    input={<Input id="select-multiple-chip" />}
                    renderValue={selected => (
                        <div className={classes.chips}>
                            {selected.map(value => {
                                if (value === 'Nothing to Select' ||
                                    typeof (value) === 'undefined') return true;
                                return (
                                    <Chip
                                        key={value}
                                        stateProperty={props.stateProperty}
                                        label={value}
                                        className={classes.chip}
                                        handleDelete={props.handleDelete}
                                    />
                                )
                            })}
                        </div>
                    )}
                    MenuProps={MenuProps}
                >
                    {MenuOptions}
                </Select>
                <FormHelperText>
                    {props.helperText}
                    {helpLink}
                </FormHelperText>
                < Transition
                    show={props.tipTrigger}
                >
                    <FormHelperText>
                        <span className={css.Tip}>
                            <strong>{props.tipText}</strong>
                        </span>
                    </FormHelperText>
                </ Transition>
            </FormControl>
        </div >
    );
}

MultipleSelect.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    options: PropTypes.array.isRequired,
    selValues: PropTypes.array.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleDelete: PropTypes.func.isRequired,
    stateProperty: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    helpLink: PropTypes.string,
    helperText: PropTypes.string,
    tipTrigger: PropTypes.bool,
    tipText: PropTypes.string
};

export default withStyles(styles, { withTheme: true })(MultipleSelect);