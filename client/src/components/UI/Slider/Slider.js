import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import { FormHelperText } from 'material-ui/Form';
import blue from 'material-ui/colors/blue';

import HelpLink from '../../../components/HelpLink/HelpLink';
import Transition from '../../../components/Transition/Transition';
import css from './Slider.module.css';

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const SliderWithToolTip = createSliderWithTooltip(Slider);

const SliderComp = props => {
    const tipSign = props.tipSign ? props.tipSign : '';

    const helpLink = props.helpLink ?
        <HelpLink helpLink={props.helpLink} /> : null;

    const helpBlock = props.helperText ? (
        <FormHelperText>
            {props.helperText}
            {helpLink}
        </FormHelperText>
    ) : null;

    const sliderHeader = props.sliderHeader ? (
        <div className={css.SliderHeader}>
            {props.sliderHeader}
        </div>
    ) : null;

    return (
        <>
            {sliderHeader}
            <SliderWithToolTip
                handleStyle={[{
                    borderWidth: '2px',
                    borderColor: blue[500],
                    backgroundColor: blue[400],
                    width: '20px',
                    height: '20px',
                    marginTop: '-8px'
                }]}
                trackStyle={[{ backgroundColor: blue[500] }]}
                tipFormatter={value => `${value}${tipSign}`}
                {...props}
            />
            {helpBlock}
            < Transition
                show={props.tipTrigger}
            >
                <FormHelperText>
                    <span className={css.Tip}>
                        <strong>{props.tipText}</strong>
                    </span>
                </FormHelperText>
            </Transition>
        </>
    );
};

SliderComp.propTypes = {
    tipSign: PropTypes.string,
    helpLink: PropTypes.string,
    helperText: PropTypes.string,
    sliderHeader: PropTypes.string,
    onAfterChange: PropTypes.func.isRequired,
    tipTrigger: PropTypes.bool,
    tipText: PropTypes.string
};

export default SliderComp;