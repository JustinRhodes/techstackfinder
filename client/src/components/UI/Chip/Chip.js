import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Chip from 'material-ui/Chip';
import CancelIcon from 'material-ui-icons/Cancel';

const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit / 4,
    },
});

const ChipComp = props => {
    const { classes, label } = props;
    return (
        <div className={classes.root}>
            <Chip
                label={label}
                onDelete={() => props.handleDelete(props.stateProperty, label)}
                className={classes.chip}
                deleteIcon={<CancelIcon />}
            />
        </div>
    );
};

ChipComp.propTypes = {
    classes: PropTypes.object.isRequired,
    stateProperty: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
};

export default withStyles(styles)(ChipComp);