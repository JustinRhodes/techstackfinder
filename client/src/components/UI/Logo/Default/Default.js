import React from 'react';
import PropTypes from 'prop-types';
import Logo from '../../../../assets/logo.png'

const LogoComp = props => {
  return (
    <img
      src={Logo}
      alt="Logo"
      style={{
        height: props.height,
        width: props.width
      }}
    />
  );
};

LogoComp.propTypes = {
  height: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
};

export default LogoComp;
