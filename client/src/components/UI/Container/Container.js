import React from 'react';
import PropTypes from 'prop-types';
import css from './Container.module.css';

const ContainerComp = props => {
    let container = (
        <div className={css.Container}>
            {props.children}
        </div>
    )

    if (props.background) {
        container = (
            <div
                className={css.Background}
                style={{ background: props.background }}
            >
                <div className={css.Container}>
                    {props.children}
                </div>
            </div>
        )
    }
    return container;
};

ContainerComp.propTypes = {
    background: PropTypes.string
};

export default ContainerComp;