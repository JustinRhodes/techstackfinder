import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { Link, withRouter } from 'react-router-dom';
import AppBar from 'material-ui/AppBar';

import Button from '../../../components/UI/Button/Ghost/Ghost';
import Logo from '../../../components/UI/Logo/Default/Default'
import * as css from './Navbar.module.css';

const styles = {
    root: {
        flexGrow: 1,
    }
};

const NavBar = props => {
    const { classes } = props;
    return (
        <div className={classes.root}>
            <AppBar position="fixed" className={css.NavBar}>
                <div className={css.LogoWrapper}>
                    <Logo height={'40px'} width={'40px'} />
                    <div className={css.Title}>TechStackFinder</div>
                </div>
                <nav className={css.MainNav}>
                    <ul className={css.MainNavItems}>
                        <li className={css.MainNavItem}>
                            <Button
                                onClick={() => props.history.push('/calculator/step1')}
                            >
                                Try it out!
                            </Button>
                        </li>
                        <li className={css.MainNavItem}>
                            <Link to={'/contact'} className={css.Link} >
                                Contact
                            </Link>
                        </li>
                    </ul>
                </nav>
            </AppBar>
        </div >
    );
}

NavBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(NavBar));