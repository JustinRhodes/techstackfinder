
import React from 'react';
import PropTypes from 'prop-types';
import {
    AreaChart,
    Area,
    XAxis,
    YAxis,
    CartesianGrid,
    ResponsiveContainer,
    Tooltip
} from 'recharts';

import css from './Stream.module.css';

const StreamComp = props => {
    const monthlyBudget = props.array.map(month => {
        return (
            {
                month: month.value,
                budget: props.data[month.value] || 0
            }
        )
    })
    return (
        <div className={css.Wrapper}>
            <ResponsiveContainer width='100%' height='100%'>
                <AreaChart data={monthlyBudget}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey={props.xKey} />
                    <YAxis />
                    <Tooltip />
                    <Area
                        type='monotone'
                        dataKey={props.yKey}
                        stroke='#8884d8'
                        fill='#8884d8' />
                </AreaChart>
            </ResponsiveContainer>
        </div>
    );
}

StreamComp.propTypes = {
    array: PropTypes.array.isRequired,
    xKey: PropTypes.string.isRequired,
    yKey: PropTypes.string.isRequired
};

export default StreamComp;