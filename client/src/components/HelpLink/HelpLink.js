import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import * as css from './HelpLink.module.css';

const HelpLink = props => {
    return (
        <>
            {' More info '}
            < Link to={props.helpLink} className={css.Link} >
                <strong>here</strong>
            </Link>
        </>
    );
};

HelpLink.propTypes = {
    helpLink: PropTypes.string.isRequired
};

export default HelpLink;