import React from 'react';
import Navbar from '../../components/UI/Navbar/Navbar';

import css from './Layout.module.css';

const Layout = props => {
  return (
    <>
      <Navbar />
      <main className={css.Content}>{props.children}</main>
    </>
  )
}

export default Layout;