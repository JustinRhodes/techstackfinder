import React from 'react';
import PropTypes from 'prop-types';
import Transition from 'react-transition-group/Transition';

const duration = 300;

const defaultStyle = {
    transition: `opacity ${duration}ms ease-in-out`,
    opacity: 0,
    width: '100%'
}

const transitionStyles = {
    entering: { opacity: 0 },
    entered: { opacity: 1 },
};

const TransitionComp = props => {
    return (
        <Transition
            in={props.show}
            timeout={duration}
            mountOnEnter
            unmountOnExit
        >
            {state => (
                <div style={{
                    ...defaultStyle,
                    ...transitionStyles[state]
                }}>
                    {props.children}
                </div>
            )}
        </Transition>
    );
};

TransitionComp.propTypes = {
    show: PropTypes.bool
};

export default TransitionComp;