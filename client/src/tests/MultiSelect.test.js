import React from 'react';
import { compose } from 'redux';
import { render, shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import ReactRouterEnzymeContext from 'react-router-enzyme-context';

import MultiSelect from '../components/UI/Select/Multi/Multi';
import './shared/setupTests';
import * as test from './shared/testData';

const expectToJson = compose(expect, toJson);
let _handleChange;
let _handleDelete;
let ctx;
let component;

beforeEach(() => {
    _handleChange = jest.fn();
    _handleDelete = jest.fn();
    ctx = new ReactRouterEnzymeContext();
});

describe('<MultiSelect /> render tests', () => {
    beforeEach(() => {
        component = render(
            <MultiSelect
                options={test.options}
                selValues={test.selValues}
                handleChange={_handleChange}
                handleDelete={_handleDelete}
                stateProperty={'test store property'}
                label={'test label'}
                helperText={'test helper text'}
                helpLink={'/test'}
                tipTrigger={false}
                tipText={`test tip`}
            />, ctx.get()
        );
    });
    it('should render correctly', () => {
        expectToJson(component)
            .toMatchSnapshot();
    });
    it('should have the right number of chips', () => {
        const chipNumber = component
            .find('span[class*="MuiChip-label"]')
            .length;
        expect(chipNumber)
            .toBe(3);
    });
    it('should have tip hidden if tipTrigger = false', () => {
        const tip = component
            .find('.Tip')
            .length
        expect(tip)
            .toBe(0);
    });
})

describe('<MultiSelect /> render tests', () => {
    beforeEach(() => {
        component = mount(
            <MultiSelect
                options={test.options}
                selValues={test.selValues}
                handleChange={_handleChange}
                handleDelete={_handleDelete}
                stateProperty={'test store property'}
                label={'test label'}
                helperText={'test helper text'}
                helpLink={'/test'}
                tipTrigger={false}
                tipText={`test tip`}
            />, ctx.get()
        );
    })
    it('should invoke handleDelete', () => {
        component
            .find('svg')
            .first()
            .simulate('click');
        expect(_handleDelete)
            .toBeCalled();
    });
})