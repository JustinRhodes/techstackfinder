export const options = [
    { value: 't1', label: 'test1' },
    { value: 't2', label: 'test2' },
    { value: 't3', label: 'test3' }
]

export const selValues = ['t1', 't2', 't3']