import requestAnimationFrame from './tempPolyfills';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
configure({ adapter: new Adapter(), disableLifecycleMethods: true });

