const frisby = require('frisby');

describe('API endpoints', () => {
    it('[v1/calculator/step1] should return status 200 with right input', () => {
        return frisby
            .post('http://localhost:5000/v1/calculator/step1', {
                bigData: 'test',
                ptOption: 'test',
                customLevel: 0,
                mvpComplexity: 0,
                users: 0,
                mvpRange: [],
                prefLang: [],
                prjData: [],
                prjFunc: [],
                prjHost: [],
                prjRange: [],
            })
            .expect('status', 200)
            .then(res => {
                let calcId = res._json.id;
                return frisby
                    .post('http://localhost:5000/v1/calculator/step2', {
                        calcId,
                        deadlines: {},
                        year1MonthExp: {},
                        year2MonthExp: {},
                    })
                    .expect('status', 200);
            });
    });
});