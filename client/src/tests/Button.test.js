import React from 'react';
import { compose } from 'redux';
import toJson from 'enzyme-to-json';
import { shallow } from 'enzyme';

import DefaultBtn from '../components/UI/Button/Default/Default';
import GhostBtn from '../components/UI/Button/Ghost/Ghost';
import './shared/setupTests';

const expectToJson = compose(expect, toJson);
let _click;
let component;

beforeEach(() => {
    _click = jest.fn();
})

describe('<DefaultButton />', () => {
    beforeEach(() => {
        component = shallow(
            <DefaultBtn onClick={_click}>
                Test
            </DefaultBtn>
        )
    });
    it('should render correctly', () => {
        expectToJson(component)
            .toMatchSnapshot();
    });
    it('should invoke onClick', () => {
        component
            .find('WithStyles')
            .simulate('click');
        expect(_click)
            .toBeCalled()
    });
})

describe('<GhostButton />', () => {
    beforeEach(() => {
        component = shallow(
            <GhostBtn onClick={_click}>
                Test
            </GhostBtn>
        )
    });
    it('should render correctly', () => {
        expectToJson(component)
            .toMatchSnapshot();
    });

    it('should invoke onClick', () => {
        component
            .find('WithStyles')
            .simulate('click');
        expect(_click)
            .toBeCalled();
    });
})
