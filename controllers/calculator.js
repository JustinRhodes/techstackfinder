
const mongoose = require('mongoose'); // needed to import User model class
const { validationResult } = require('express-validator/check');
var ObjectId = require('mongodb').ObjectID;

const Calc = mongoose.model('calcInput');
const utility = require('../services/utility');
const config = require('../config');

const internalErrorHandler = (err, res) => {
  // console.error('DB error creating user document:', err);
  return res
    .status(500)
    .send(utility.msg.internalServerError);
}

const validationErrorHandler = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).send(utility.msg.invalidInput);
  }
}

exports.step1 = {
  // Saving calculator input in new doc
  post: (req, res) => {
    validationErrorHandler(req, res);
    const calc = new Calc({
      bigData: req.body.bigData,
      customLevel: req.body.customLevel,
      mvpComplexity: req.body.mvpComplexity,
      mvpRange: req.body.mvpRange,
      prefLang: req.body.prefLang,
      prjData: req.body.prjData,
      prjFunc: req.body.prjFunc,
      prjHost: req.body.prjHost,
      prjRange: req.body.prjRange,
      ptOption: req.body.ptOption,
      users: req.body.users
    });
    calc.save((err, newCalc) => {
      if (err) internalErrorHandler(err, res);
      res.status(200).json({
        'msg': utility.msg.success,
        'id': newCalc._id
      });
    });
  }
};

exports.step2 = {
  // Saving calculator input in existing doc
  post: (req, res) => {
    validationErrorHandler(req, res);
    let calcId = ObjectId(req.body.calcId);
    Calc.findOne({ _id: calcId }, (err, calc) => {
      if (err || !calc) internalErrorHandler(err, res);
      calc.deadlines = req.body.deadlines;
      calc.year1MonthExp = req.body.year1MonthExp;
      calc.year2MonthExp = req.body.year2MonthExp;
      calc.save((err, newCalc) => {
        if (err) internalErrorHandler(err, res);
        res.status(200).send(utility.msg.success);
      })
    })
  }
};



