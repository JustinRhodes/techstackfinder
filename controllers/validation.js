const { body } = require('express-validator/check');
const config = require('../config');

exports.step1 = [
  body('bigData')
    .custom(value => typeof value === 'string'),
  body('ptOption')
    .custom(value => typeof value === 'string'),
  body('customLevel').isInt(),
  body('mvpComplexity').isInt(),
  body('users').isInt(),
  body('mvpRange')
    .custom(value => Array.isArray(value)),
  body('prefLang')
    .custom(value => Array.isArray(value)),
  body('prjData')
    .custom(value => Array.isArray(value)),
  body('prjFunc')
    .custom(value => Array.isArray(value)),
  body('prjHost')
    .custom(value => Array.isArray(value)),
  body('prjRange')
    .custom(value => Array.isArray(value)),
];

exports.step2 = [
  body('deadlines')
    .custom(value => typeof value === 'object'
      && !Array.isArray(value)),
  body('year1MonthExp')
    .custom(value => typeof value === 'object'
      && !Array.isArray(value)),
  body('year2MonthExp')
    .custom(value => typeof value === 'object'
      && !Array.isArray(value)),
];
