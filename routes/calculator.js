const express = require('express');
const router = express.Router();

const calcCtrl = require('../controllers/calculator');
const validCtrl = require('../controllers/validation');

//## Calculator-related routes ##//
router.route('/step1')
    .post(validCtrl.step1, calcCtrl.step1.post);
router.route('/step2')
    .post(validCtrl.step2, calcCtrl.step2.post);

module.exports = router;
