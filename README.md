# TechstackFinder v. 1.0.0

TechstackFinder is an online techstack consultancy website focusing on startups.

## Getting Started

These instructions will get the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### DevTools

* [Redux DevTools (Chrome Addon)](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en) - Redux DevTools for debugging application's state changes.

### Prerequisites

* [Git](https://git-scm.com/) - Version control software
* [Node.js](https://nodejs.org/en/download/current/) - JS runtime environment
* [Npm](https://www.npmjs.com/) - Package manager (comes with node)
* [MongoDB](https://www.mongodb.com/) - NoSql database used for this project
* [create-react-app](https://github.com/facebook/create-react-app) - Dev environment. **CAREFUL:** The project uses the alpha version 2.0.0 to allow for css modules (more info [here](https://github.com/facebook/create-react-app/pull/2285) & [here](https://github.com/css-modules/css-modules)). Note: as soon as a more stable v2.0.0 has been released the project will be upgraded. You can install/upgrade the latest alpha version of react-scripts using one of thefollowing commands below (see also [here](https://github.com/facebook/create-react-app/issues/3815)).

```txt
# Create a new application
npx create-react-app@next --scripts-version=2.0.0-next.47d2d941

# Upgrade an existing application
yarn upgrade react-scripts@2.0.0-next.47d2d941
```

### Installation & first-time setup

* Install [Git](https://git-scm.com/), [Node.js](https://nodejs.org/en/download/current/) & [MongoDB](https://www.mongodb.com/) on your local OS.
* In the **cmnd prompt**, run `md \data\db` (Windows) or `mkdir -p /data/db` (macOS) to create a folder to hold the DB data.
* In the **cmnd prompt**, navigate to the folder where you want to download the Bitbucket repository to. Then clone the remote repository from Bitbucket by typing the following in your **cmnd prompt** (cloning is only done once, to update your local repo use pull described later): `git clone https://bitbucket.org/JustinRhodes/techstackfinder.git`. You can find your clone link on the Bitbucket repository.
* In the **cmnd prompt**, navigate to the project's **root** folder & run `npm install` to install the server-side 3rd party software.
* In the **cmnd prompt**, navigate to the project's **client** folder & run `npm install` to install the client-side 3rd party software.

### Running & managing the code

* Start the MongoDB database by running `"C:\Program Files\MongoDB\Server\3.6\bin\mongod.exe" --dbpath "d:\test\mongo db data"` (Windows) or `mongod` (macOS) in the **cmnd prompt**.
* Ensure your local repository is up-tp-date with the Bitbucket repository. To do this navigate in your **cmd prompt** to the **root** folder & type the following: `git pull https://bitbucket.org/JustinRhodes/techstackfinder.git`. You can find your download link on the Bitbucket repository.
* In the **cmnd prompt**, navigate to the project's **root** folder & run `npm update`. This makes sure all the **server-side** 3rd party software is up-to-date.
* In the **cmnd prompt**, navigate to the project's **client** folder & also run `npm update` to update any **client-side** 3rd party software.
* Navigate back to the **root** folder to run `npm run dev` & wait for the development servers to start.
* Your standard browser should now open. If not, open your browser and type `http://localhost:3000/` in the browser's navigation bar.

## API Documentation

**Description** : A full API documentation (Swagger) is available via the dev server using the below URL.

**URL** : `http://localhost:3000/v1/redoc`


## Deployment

Additional notes about how to deploy this on a live system will be added here.

## Built With (amongst Others)

* [React](https://github.com/facebook/react) - JS front end Framework
* [Redux](https://redux.js.org/) - State Management
* [Axios](https://github.com/axios/axios) - HTTP client
* [Node.js](https://nodejs.org/en/download/current/) - JS runtime environment
* [Express](https://github.com/expressjs/expressjs.com) - JS server framework
* [express-validator](https://github.com/ctavan/express-validator) - Server-side input validation
* [Mongoose](http://mongoosejs.com/) - MongoDB driver
* [create-react-app](https://github.com/facebook/create-react-app) - Development environment

## Version Controle

[Git](https://git-scm.com/) is used for version controle. For the code versions available, see the different branches on this bitbucket repository.

## Authors

* **Justin Rhodes** - _Front & Back end_ - [JR Consult Ltd.](https://jrconsult.com)

## License

This project is proprietary - see the [LICENSE.md](LICENSE.md) file for details.
