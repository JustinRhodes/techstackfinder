Copyright © 2018 JR Consult Ltd. - All Rights Reserved

---

* Unauthorized copying of this file, via any medium is strictly prohibited
* All files in this repository are copyrighted and may not be copied or shared without prior authorization by JR Consult Ltd.
* Written by [Justin Rhodes](mailto:justin.rhodes@reflinx.com), April 2018
